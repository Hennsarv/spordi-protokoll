﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp19
{
    static class Program
    {

        static void Main(string[] args)
        {
            // ülesanne oli:
            // leida kõige kiirem sprinter (100m jooksja)
            // leida kõige kiirem jooksja (ehk mõni jookseb 400 kiiremini kui 100)
            // leida kõige stabiilsem jooksja
            //      * jookseb vähemalt 2 distantsi
            //      * kiirus erinevatel distantsidel erineb kõige vähem

        


            List<Tulemus> tulemused = new List<Tulemus>();
            string failinimi = @"..\..\spordipäeva protokoll.txt";
            List<string> loetudread = File.ReadAllLines(failinimi).ToList();
            loetudread.RemoveAt(0);
            foreach (var x in loetudread)
            {
                var reaOsad = x.Split(',');
                Console.WriteLine(
                    $"{reaOsad[0].Trim()} jooksis {reaOsad[1].Trim()}m ajaga {reaOsad[2].Trim()}"
                    );

                string aeg = reaOsad[2].Trim();
                if (!aeg.Contains(':')) aeg = "00:" + aeg;
                aeg = "00:" + aeg;

                tulemused.Add(new Tulemus
                {
                    Nimi = reaOsad[0].Trim()
                    ,
                    Distants = int.Parse(reaOsad[1].Trim())
                    ,
                    Aeg = TimeSpan.Parse(aeg)
                });


            }

            foreach (var x in tulemused.Where(x => x.Distants == 100))
            {
                Console.WriteLine($"{x.Nimi} {x.Distants} {x.Aeg} {x.Aeg.TotalSeconds}");
            }
            // kasutame extensioneid
            Console.WriteLine(
                tulemused
                //.Where(x => x.Distants == 100)
                .OrderBy(x => (x.Distants / x.Aeg.TotalSeconds))
                .Reverse()
                .Take(1)
                .Single().Nimi);

            // kasutame LINQ avaldisi
            Console.WriteLine((from x in tulemused
                               where x.Distants == 100
                               orderby x.Distants / x.Aeg.TotalSeconds
                               descending
                               select x
                     ).Take(1).Single().Nimi);

            // leiame kes jooksevad mitut distantsi

            var q = tulemused
                .ToLookup(x => x.Nimi, x => x)
                .Where(x => x.Count() > 2)
                .Select(x => new { Nimi = x.Key, Vahe = (x.Max(y => y.Distants / y.Aeg.TotalSeconds)) - (x.Min(y => y.Distants / y.Aeg.TotalSeconds)) })
                .OrderBy(x => x.Vahe)
                .Take(1)
                .Single();

            Console.WriteLine($"kõige yhtlasem on {q.Nimi} kiiruste vahega {q.vahe}");
        }
    }


    class Tulemus
    {
        public string Nimi;
        public int Distants;
        public TimeSpan Aeg;
    }
}
